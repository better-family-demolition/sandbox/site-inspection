var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: `boundaries-io, Leaflet, and WeatherAPI`
  });
});

module.exports = router;
