const openStreetMap = {
  'attribution': `Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>`
}

const mapboxApi = {
  accessToken: `pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw`,
  gregswindleAccessToken: `pk.eyJ1IjoiZ3JlZ3N3aW5kbGUiLCJhIjoiY2t1dzQ3NGd6MXdveTJvcGpuY3hkdGlocCJ9.ChcrWccBiRz4M7dWNUkXdg`
}

const mapFactoryDefaults = {
  'settings': {
    'view': {
      'center': {
        'lat': 42.3753017,
        'lon': -83.0742017
      },
      'zoom': 11
    },
    'geoJson': {
      'options': {

      }
    },
    'tileLayer': {
      'options': {
        'accessToken': mapboxApi.gregswindleAccessToken,
        'attribution': openStreetMap.attribution,
        'id': 'mapbox/streets-v11',
        'maxZoom': 18,
        'tileSize': 512,
        'zoomOffset': -1
      },
      'urlTemplate': `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}`
    }
  }
}

const mapFactory = {
  'settings': Object.create(mapFactoryDefaults.settings),
  
  create(id, leaflet, opts = {}) {
    Object.assign(
      this.settings,
      opts
    )
    const { center, zoom } = this.settings.view

    return leaflet.map(`${id}`).setView([
      center.lat,
      center.lon
    ], zoom)
  },

  addTileLayerTo(map, leaflet, opts = {}) {
    const {
      options,
      urlTemplate
    } = this.settings.tileLayer

    const tileOptions = Object.assign(options, opts)

    leaflet.tileLayer(
      urlTemplate,
      tileOptions
    ).addTo(map)

    return map
  },

  onEachFeature(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.popupContent) {
      layer.bindPopup(feature.properties.popupContent);
    }
  },

  renderGeoJson(id, leaflet, featureCollection) {
    var weatherMap = this.create(id, leaflet)
    this.addTileLayerTo(weatherMap, leaflet)
    leaflet.geoJson(featureCollection, {
      'onEachFeature': this.onEachFeature
    }).addTo(weatherMap)
  },

  renderWith(leaflet) {
    var weatherMap = this.create('weatherMap', leaflet)
    this.addTileLayerTo(weatherMap, leaflet)
  }
}

mapFactory.renderGeoJson('weatherMap', L, detroitMiFeatureCollection)

// module.exports = mapFactory
